== CONTRIBUTING ==

Currently NinucsWrt is released in the form of binary files, .config OpenWrt 
build system files (one for each supported devices) and needed patch files in 
the unified diff format. After checking out the matching OpenWrt revision and 
feeds, you can build identical binaries yourself by applying the applying the 
patches and the relevant config file.

We are currently exploring which is the most efficient way for us to provide a
complete source tree for each of the branches we support (ideally oldstable,
stable and trunk), and keeping those in sync with their respective upstream
OpenWrt branch. We'll get there!

In the meantime, you can:

- help defining the roadmap and with the actual development on the
Ninux-Calabria Mailing List.

- propose yourself as a device maintainer. Only requirements are that you have
an active Ninux.org node with the device running with NinucsWrt on it. This way
you can help test configurations and squash bugs!

== WHY NOT... ==

There are already a couple of firmwares developed/used in the Ninux.org
community. Here's our rationale for starting yet another project.

* Why not Scooreggione?

Scooreggione is an OpenWrt distribution with a strong adoption in the Ninux.org
original Rome island, mainly developed by Saverio "ZioProto" Proto. It stays as
close as possible to vanilla OpenWrt (which doesn't feature a Web UI), it is
released only for the current stable OpenWrt branch and for a lot of devices.
This might not always be the best choice of action for our purpose, for the
reasons above.
But most of all, Scooreggione comes with a configuration which is best suited
for hosting the routing daemon directly on the radio, while we'd like a firmware
more suited for a ground routing configuration strategy.

* Why not Libre-Mesh?

The idea of a modular firmware commonly developed by and for mesh networks
around the globe is awesome, no questions asked. Presently though we are not
able and/or willing to follow Libre-Mesh development agreements described on the
"Objectives" page. But most of all, Libre-Mesh is currently only suited for mesh
networks based on B.A.T.M.A.N. and BMX routing, while we're focusing on mesh
networks like our own, based on the OLSR routing daemon. 
But obviously we'll keep an eye on the project with the goal of a unified
firmware in mind.
